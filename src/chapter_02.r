###
# Statistical Rethinking
# Chapter 2

# Likelihood of 6 W in 9 tosses
dbinom(6, size=9, prob=0.5)

###
# Grid approximation
# R code 2.3

num.points <- 20

# define grid
p_grid <- seq(from=0, to=1, length.out = num.points)

# define prior
prior <- rep(1, num.points)
#prior <- ifelse(p_grid < 0.5, 0, 1)
#prior <- exp(-5*abs(p_grid - 0.5))

# compute likelihood at each value in grid
likelihood <- dbinom(6, size=9, prob=p_grid)

# compute product of likelihood and prior
unstd.posterior <- likelihood * prior

# standardize the posterior, so it sums to 1
posterior <- unstd.posterior / sum(unstd.posterior)

# display the posterior distribution
plot(p_grid, posterior, type="b",
     xlab="probability of water",
     ylab="posterior probability")
mtext(paste(num.points, "points"))

###
# Quadratic approximation
# R code 2.6 & 2.7
library(rethinking)
globe.qa <- map(
  alist(
    w ~ dbinom(9,p) , # binomial likelihood
    p ~ dunif(0,1)    # uniform prior
  ),
  data=list(w=6)
)

# display summary of quadratic approximation
precis(globe.qa)

# analytical calculation
w <- 6
n <- 9
curve(dbeta(x, w+1, n-w+1), from=0, to=1)
# quadratic approximation
curve(dnorm(x, 0.67, 0.16), lty=2, add=T)

# Exercice 2M3 two globes, earth 0.7W, mars 0.0W obtain L show Pr(Earth|Land) = 0.23
# Pr(Earth|Land) = Pr(Land|Earth)*Pr(Earth)/Pr(Land)
pr.land_earth = 0.3
pr.earth = 0.5
pr.land = 0.5*1 + 0.5*0.3
pr.land_earth*pr.earth/pr.land

a.twins = 0.5*0.2*0.2
a.single = 0.5*0.2*0.8
b.twins = 0.5*0.1*0.1
b.single = 0.5*0.1*0.9
(a.twins+b.twins)/(a.twins+b.twins+a.single+b.single)
