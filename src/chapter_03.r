###
# Rethinking chapter 03

library(rethinking)

p_grid <- seq(from=0, to=1, length.out = 1000)
prior <- rep(1, 1000)
likelihood <- dbinom(6, size = 9, prob = p_grid)
posterior <- likelihood * prior
posterior <- posterior / sum(posterior)

plot(samples)

dens(samples)

# R code 3.11
p_grid <- seq(from=0, to=1, length.out = 1000)
prior <- rep(1, 1000)
likelihood <- dbinom(3, size = 3, prob = p_grid)
posterior <- likelihood * prior
posterior <- posterior / sum(posterior)
samples <- sample(p_grid, prob=posterior, size=1e4, replace=T)

plot(samples)

dens(samples)


PI(samples, prob = 0.5)
HPDI(samples, prob = 0.5)

p_grid[which.max(posterior)]
chainmode(samples, adj=0.01)

loss <- sapply(p_grid, function(x)sum(posterior*abs(x-p_grid)))
p_grid[which.min(loss)]
